
let buyers = `[
    {
      "id": 2023001,
      "name": "Karl",
      "lastName": "Aquino",
      "email": "kaquino@mail.com",
      "password": "qwerty123",
      "mobile number": "0919 000 0000",

      "orders": {
        "userid": 2023001,
        "transactiondate": "03-23-2023",
        "status": "pre-order",
        "total": "Php. 30,000",

        "products": {
          "name": "MSI",
          "description": "Laptop",
          "price": "Php. 15,000",
          "stocks": "99pcs",
          "SKU": "msi27blkv1",
          "order products": {
            "order id": "1",
            "product id": "1",
            "quantity": "2 pcs.",
            "price": "Php. 15,000",
            "subtotal": "Php. 30,000"
          }
        }
      }     
    },
 {
   "id": 2023002,
   "name": "Ian",
   "lastName": "Valeroso",
   "email": "ivaleroso@mail.com",
   "password": "asdfgh123",
   "mobile number": "0927 000 0000",

   "orders": {
     "userid": 2023002,
     "transactiondate": "03-28-2023",
     "status": "pick-up",
     "total": "Php. 100,000",

     "products": {
       "name": "ASUS",
       "description": "GPU",
       "price": "Php. 70,000",
       "stocks": "10pcs",
       "SKU": "asusrtx3080ti",
       "order products": {
         "order id": "2",
         "product id": "2",
         "quantity": "1 pc.",
         "price": "Php. 70,000",
         "subtotal": "Php. 70,000"
        },

          "products": {
       "name": "asus",
       "description": "monitor",
       "price": "Php. 30,000",
       "stocks": "42pcs",
       "SKU": "asus32rogwhite",
       "order products": {
         "order id": "1",
         "product id": "1",
         "quantity": "1 pc.",
         "price": "Php. 30,000",
         "subtotal": "Php. 30,000"
        }
      }
    }     
 }
}
]`;

console.log(JSON.parse(buyers));
